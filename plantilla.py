#!/usr/bin/env pythoh3

'''
Program to order a list of words given as arguments
'''

import sys


def is_lower(first: str, second: str):
    """Return True if first is lower (alphabetically) than second

    Order is checked after lowercasing the letters
    `<` is only used on single characteres.
    """
    x= first.lower()
    y= second.lower()
    if x<y:
        lower= True
    else:
        lower= False
    return lower

def get_lower(words: list, pos: int):
    """Get lower word, for words right of pos (including pos)"""
    lower=pos

    for i in range(pos, len(words)):
        if is_lower(words[pos], words[i])== False:
            pos=i
    lower=pos
    return lower
def sort(words: list):
    """Return the list of words, ordered alphabetically"""
    lista=[]
    for i in range (0,len(words)):
        num==get_lower(words,i)
        lista +=(words(num))
        words(num)==words(i)
    print(lista)
    return lista

def show(words: list):
    """Show words on screen, using print()"""
    return print(words)

def main():
    words: list = sys.argv[1:]
    ordered: list = sort(words)
    show(ordered)


if __name__ == '__main__':
    main()
